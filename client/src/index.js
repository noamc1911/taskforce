import React from 'react';
import ReactDOM from 'react-dom';
import App from './routes/index.js';
import './styles.css';

const app = document.getElementById('app');

ReactDOM.render(<App />, app);